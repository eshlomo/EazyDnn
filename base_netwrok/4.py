import matplotlib.pyplot as plt
import pylab
import numpy as np
import base_netwrok.utils as u

def sigmoid(z):
    return 1 / (1 + np.exp(-z))

def sigmoidPrime(z):
    return np.exp(-z) / ((1 + np.exp(-z))**2)


sin_exp = lambda x: 2.5*np.exp(-1.0*x/2)*np.cos(np.pi*x)
line=lambda x:x
X, Y = u.generate_pattern(0, 10, 1000, line)
test = u.generate_test(0, 10, 1000, line)
plt.plot(X, Y,'co')
plt.title("Generated training data")
plt.show()


class DNN(object):
    def __init__(self, seed=1, inSize=1, hiddenSize=3, outSize=1):
        self.inputSize = inSize
        self.hiddenSize = hiddenSize
        self.outputSize = outSize
        np.random.seed(seed)
        self.W1 = np.random.randn(self.inputSize, self.hiddenSize)
        self.W2 = np.random.randn(self.hiddenSize, self.outputSize)

    def forward(self, X):
        self.z2 = np.dot(X, self.W1)
        self.a2 = sigmoid(self.z2)
        self.z3 = np.dot(self.a2, self.W2)
        out = sigmoid(self.z3)
        return out

    def cost(self, X, y):
        self.yHat = self.forward(X)
        return np.sum(0.5 * ((y - self.yHat) ** 2))

    def costPrime(self, X, y):
        self.yHat = self.forward(X)
        d3 = np.multiply(-(y - self.yHat), sigmoidPrime(self.z3))
        dJdW2 = np.dot(self.a2.T, d3)
        d2 = np.dot(d3, self.W2.T) * sigmoidPrime(self.z2)
        dJdW1 = np.dot(X.T, d2)
        return dJdW1, dJdW2

    def getParams(self):
        params = np.concatenate((self.W1.ravel(), self.W2.ravel()))
        return params

    def setParams(self, params):
        W1_start = 0
        W1_end = self.inputSize * self.hiddenSize
        self.W1 = np.reshape(params[W1_start:W1_end], (self.inputSize, self.hiddenSize))
        W2_end = W1_end + self.hiddenSize * self.outputSize
        self.W2 = np.reshape(params[W1_end:W2_end], (self.hiddenSize, self.outputSize))

    def computeGradients(self, X, y):
        dJdW1, dJdW2 = self.costPrime(X, y)
        return np.concatenate((dJdW1.ravel(), dJdW2.ravel()))

nn = DNN(inSize=1,hiddenSize=30)
u.test_line(nn,X,Y,test)
