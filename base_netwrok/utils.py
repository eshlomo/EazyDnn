import numpy as np
import matplotlib.pyplot as plt


def generate_pattern(start_x=0, end_x=1, num_smaples=50, generator=lambda x: x):
    y = np.array(([75], [82], [93]), dtype=float)
    x_series = []
    y_series = []
    interval = (end_x - start_x) / num_smaples
    for i in range(0, num_smaples):
        x=start_x+i * interval
        y=generator(x)
        x_series.append(x)
        y_series.append(y)
    x_series = np.array([x_series], dtype=float)
    y_series = np.array([y_series], dtype=float)
    x_series = x_series.T
    y_series = y_series.T
    return x_series, y_series

def generate_test(start_x=0, end_x=1, num_smaples=50, generator=lambda x: x):
    y = np.array(([75], [82], [93]), dtype=float)
    x_series = []
    y_series = []
    interval = (end_x - start_x) / num_smaples
    for i in range(0, num_smaples):
        x_series.append(i * interval)
        y_series.append(generator(i * interval))
    x_series=np.array([(x_series)], dtype=float)
    y_series=np.array([(y_series)], dtype=float)
    x_series=x_series.T
    y_series=y_series.T
    return x_series

class linear_trainer(object):
    def __init__(self, N):
        # Make Local reference to network:
        self.N = N

    def callbackF(self, params):
        self.N.setParams(params)
        self.J.append(self.N.cost(self.X, self.y))

    def costFunctionWrapper(self, params, X, y):
        self.N.setParams(params)
        cost = self.N.cost(X, y)
        grad = self.N.computeGradients(X, y)
        return cost, grad

    def train(self, X, y, iterations=10000, lr=.01):
        print("Training for {0}".format(iterations))
        # Make an internal variable for the callback function:
        self.X = X
        self.y = y

        # Make empty list to store costs:
        self.J = []
        for i in range(iterations):
            params = self.N.getParams()
            cost = self.N.cost(X, y)
            grad = self.N.computeGradients(X, y)
            self.J.append(cost)
            params=params-lr*grad
            self.N.setParams(params)

## make sure you install scipy ##
from scipy import optimize


class BFGS_trainer(object):
    def __init__(self, N,max_iter=1000):
        # Make Local reference to network:
        self.N = N
        self.max_iter=max_iter

    def callbackF(self, params):
        self.N.setParams(params)
        self.J.append(self.N.cost(self.X, self.y))

    def costFunctionWrapper(self, params, X, y):
        self.N.setParams(params)
        cost = self.N.cost(X, y)
        grad = self.N.computeGradients(X, y)
        return cost, grad

    def train(self, X, y):
        # Make an internal variable for the callback function:
        self.X = X
        self.y = y

        # Make empty list to store costs:
        self.J = []

        params0 = self.N.getParams()

        options = {'maxiter': self.max_iter, 'disp': True}
        _res = optimize.minimize(self.costFunctionWrapper, params0, jac=True, method='BFGS', \
                                 args=(X, y), options=options, callback=self.callbackF)

        self.N.setParams(_res.x)
        self.optimizationResults = _res

def test_line(nn,x,y,test):
    '''
    
    :param nn:this is your neural network class instance  
            x: the x values vector as returned from generate pattern 
            y: the x values vector as returned from generate pattern 
            test: the test values vector as returned from generate test
    :return: 
    '''

    # Normalize
    x = x / np.amax(x, axis=0)
    y = y / np.amax(y, axis=0)
    fig1 = plt.figure()
    ax1 = fig1.add_subplot(111)
    ax1.plot(x, y)
    ax1.set_title("Prediction VS ground truth")
    T = linear_trainer(nn)
    T.train(x, y)
    ax1.plot(test, T.N.forward(test))
    # draw cost
    fig2 = plt.figure()
    ax2 = fig2.add_subplot(111)
    ax2.plot(T.J)
    ax2.set_title("Cost over iterations")
    plt.show()




''' Methods you should add to your class'''


def getParams(self):
    '''
    
    :param self: 
    :return: our wegiht matrices are a singe 1 d vector. 
    '''
    params = np.concatenate((self.W1.ravel(), self.W2.ravel()))
    return params


def setParams(self, params):
    '''
    
    :param self: 
    :param params: take the vector of the params a 1 dimentional and span it according to size.   
    :return: 
    '''
    W1_start = 0
    W1_end = self.inputSize * self.hiddenSize
    self.W1 = np.reshape(params[W1_start:W1_end], (self.inputSize, self.hiddenSize))
    W2_end = W1_end + self.hiddenSize * self.outputSize
    self.W2 = np.reshape(params[W1_end:W2_end], (self.hiddenSize, self.outputSize))


def computeGradients(self, X, y):
    '''
    
    :param self: 
    :param X:Inputs 
    :param y: Outputs
    :return: 1d array of our derivatives 
    '''
    dJdW1, dJdW2 = self.costPrime(X, y)
    return np.concatenate((dJdW1.ravel(), dJdW2.ravel()))