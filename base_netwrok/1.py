import matplotlib.pyplot as plt
import pylab
import numpy as np


def sigmoid(z):
    return 1 / (1 + np.exp(-z))


X = 10 * np.random.rand(100) - 5
Y = sigmoid(X)
plt.plot(X, Y,'co')
plt.show()
