import matplotlib.pyplot as plt
import pylab
import numpy as np
import base_netwrok.utils as u

def sigmoid(z):
    return 1 / (1 + np.exp(-z))


X,Y = u.generate_pattern(-1,1,100,lambda x: x**2)
plt.plot(X, Y)
plt.show()
