import numpy as np
import matplotlib.pyplot as plt
np.random.seed(1234) # init random seed for determinism
A = np.random.randn(100)
plt.figure(1)
plt.subplot(211)
plt.plot(A)
plt.subplot(212)
plt.hist(A)
plt.show()
