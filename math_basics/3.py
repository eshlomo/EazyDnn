import numpy as np
import matplotlib.pyplot as plt

np.random.seed(1234)  # init random seed for determinism
size_m = 10
A = np.random.randn(size_m, size_m)
B = 4 * np.random.randn(size_m, size_m) + 3


def mul_matrix(a, b):
    result = np.zeros((a.shape[0], b.shape[1]))
    row_ind = 0
    for row in a:
        col_ind = 0
        for col in b.T:
            for i in range(row.shape[0]):
                result[row_ind, col_ind] += row[i] * col[i]
            col_ind += 1
        row_ind += 1
    return result


print(mul_matrix(A, B) == np.dot(A, B))

'''plt.figure(1)
plt.subplot(211)
plt.hist(A.flatten())
plt.subplot(212)
plt.hist(B.flatten())
plt.show()
'''
