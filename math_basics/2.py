import numpy as np
import matplotlib.pyplot as plt
np.random.seed(1234) # init random seed for determinism
A = np.random.randn(10,10)
B = 4*np.random.randn(10,10)+3
plt.figure(1)
plt.subplot(211)
plt.hist(A.flatten())
plt.subplot(212)
plt.hist(B.flatten())
plt.show()
